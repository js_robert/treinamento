package com.indracompany.treinamento.model.repository;

import com.indracompany.treinamento.model.entity.Cliente;

public interface ClienteRepository extends GenericCrudRepository<Cliente, Long> {

    Cliente findByCpf(String cpf);

    Cliente findByEmail(String email);

    Cliente findByNome(String nome);
    
    public boolean existsByCpf(String cpf);
    
    public boolean existsByEmail(String email);

}
